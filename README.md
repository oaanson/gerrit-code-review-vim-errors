Read Gerrit code review commends and write them to a error-list format usable
in VIM. For use, add the following to your vimrc:
```
command -nargs=* -bang GCR cexpr<bang> system("gerrit-code-review-vim-errors.sh gerrit:8080 <args>")
```
Replace gerrit:8080 above with the URL of your gerrit server. Then the command:
```
:GCR[!] <Gerrit Patch ID>
```
Will place the comments in your VIM's error file.

It also takes the Change-Id automatically from the top-most commit if the
Gerrit Patch ID is not given:
```
:GCR
```
