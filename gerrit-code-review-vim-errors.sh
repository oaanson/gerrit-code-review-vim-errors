#!/bin/bash

USAGE="USAGE: $0 <gerrit server> [<change-id>] [<revision>]"
GERRIT=${1:?$USAGE}
if [ "$GERRIT" == "-h" ]; then
	echo "USAGE: $0 <gerrit server> [<change-id>] [<revision>]"
	echo -e "\tgerrit server:  The Gerrit server"
	echo -e "\tchange-id:      The change ID in review (e.g., 1234)"
	echo -e "\t                Default: Taken from commit message"
	echo -e "\trevision:       The revision to take comments (e.g., 3)"
	echo -e "\t                Use 'current' for last revision"
	echo -e "\t                Default: all"
	exit 0
fi
CHANGEID=${2:-""}
if [ -z "$CHANGEID" ]; then
	CHANGEID=$(git log -1 | awk '$1 == "Change-Id:" {print $2}')
fi
if [ -z "$CHANGEID" ]; then
	echo >&2 "$USAGE"
	exit 1
fi

REVISION=${3:-""}
if [ -n "$REVISION" ]; then
	if [ "$REVISION" = "all" ]; then
		REVISION=
	else
		REVISION=/revisions/$REVISION
	fi
fi

tmpfile=$(mktemp)
cat << "EOF" > $tmpfile
import json
import sys

class GerritComments:
    def __init__(self, jsonstream):
        self._data = json.load(jsonstream)
        self._comments_map = dict((comment["id"], comment)
                                  for _file, comment in self.comments())
        self._mark_resolved_comments()

    def comments(self):
        for file, comments in self._data.items():
            for comment in comments:
                yield file, comment

    def _mark_resolved_recursive(self, comment):
        comment["unresolved"] = False
        try:
            self._mark_resolved_recursive(
                self._comments_map[comment["in_reply_to"]])
        except KeyError:
            pass  # Return

    def _mark_resolved_comments(self):
        for comment in self._comments_map.values():
            if not comment["unresolved"]:
                self._mark_resolved_recursive(comment)


gerrit_comments = GerritComments(sys.stdin)
for file, comment in gerrit_comments.comments():
    if not comment["unresolved"]:
        continue
    print(f'{file}:{comment["line"]}: {comment["author"]["name"]}: '
          f'{"(Resolved)" if not comment.get("unresolved", False) else ""}')
    print(comment['message'])
    print('---------------------------------------------------------------------------')
    print(' ')
EOF

curl $GERRIT/changes/$CHANGEID$REVISION/comments/ -s | tail -n +2 | python $tmpfile
rm -f $tmpfile

